export default {

    /**
     * Send content with its format and expected output format to retrieve the content converted to the expected format
     * @param {JSON} dataToConvert JSON object with content type, expected output type and content
    ```
    dataToConvert: {
        textToConvert: {string}, // content to convert
        inputFormat: {string}, // content type
        outPutFormat: {string} // expected output type
    }
    ```
     * @returns {Promise<Blob>} a promise of the expected converted content
     * @author bdelseny
     */
    composeDocument(dataToConvert) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        return fetch(`http://localhost:3000/compose`, {
            method: "POST",
            mode: "cors",
            cache: 'default',
            headers: myHeaders,
            body: JSON.stringify(dataToConvert)
        })
    }
}