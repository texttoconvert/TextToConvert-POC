# TextToConvert POC

TextToConvert aims to be a document composition interface.  
Its embeds free software such as [pandoc](https://pandoc.org/) and [Texlive](https://www.tug.org/texlive/) as document compiler and converter.

## POC objectives

The POC objective was to see if the [TextToConvert](https://gitlab.com/texttoconvert) project was worth it and possible to make.  

## POC results

The POC shows that TextToConvert is possible to make. And for what it is worth, I would like to have free software like it, so it is enough for us to develop it.

## POC informations

This is far from being the final version of a polished version.  
Not everything is working.  
No data is stored or even used.  
We do not provide any guaranty on this product.  
See TextToConvert-Documentation and full TextToConvert project repositories for more information.  
More information will be provided on TextToConvert-Documentation in near future.

## Installation guide

**You need docker and docker-compose to be installed on your system.**  
*The following guide is for Linux users, if you are on Windows or MacOs, or other systems please adapt the following information. Sorry for the inconvenient.*

To install TextToConvert-POC please follow the following steps:
- Clone this repository using ```git clone```
- Go to your new ```TextToConvert-POC``` directory, ```cd TextToConvert-POC```
- Launch the app using docker compose ```docker-compose up```. If this does not work try launching docker compose as a sudo user ```sudo docker-compose up```
- Go to http://localhost:8080 URL.
- Now you should see the following screen on your browser, find usage information in the following section.

![TextToConvert screenshot](./Images/TextToConvert.png)

## Usage information

The following screenshot show you the interface elements, information on these elements are bellow the screenshot.

![TextToConvert Screenshot information](./Images/TextToConvert-ScreenshotInformation.jpg)

Screen elements:
1. ```Compile``` button: this button is used to compile the text set on the text area, elemnt 6, from the setted input format, element 2, to the wanted output format, element 3.
2. Text input: this text input sets the input format of the text written on the text area, element 6. By default the input format is set to *markdown* markup text language.
3. Text input: this text input sets the expected output format of the document. By default it is set to *HTML* and a preview is available on the preview area, element 7.
4. Link: this link, when available, permit to the user to download the compiled document.
5. Link: this link, when available, permt to the user to open the compiled document on a new browser tab.
6. Text area: this area lets the user writting what he wants. It can be compiled using the ```Compile``` button, element 1, and its writting format should correspond to the input format sets above, element 2.
7. Iframe: this iframe shows a preview of the compiled document. If the browser can handle the output format setted by the user a preview will be available on this area. On the other hand, if the browser can not handle the expected output format the browser will permit to the user to dowload the compiled document file.

## Special thanks

Big thanks to [Gaëtan Lagier](https://github.com/Kurtil), who lets me use is [awsome-app](https://github.com/Kurtil/awsome-app) as a base for this POC. Thank you Gaëtan also for you support and your help on this project.  
Big thanks to [Nicolas Richel](https://gitlab.com/NicolasRichel) for is help on this POC, his support and for showing me that this repository was a working POC and that my work was worth it.  
Thanks to you both for our awesome discussions on computer science and technology, without you I would not have been to the end of this POC.

Thanks also to you who is reading this, I hope you will find this project as cool as me.  
ENJOY!