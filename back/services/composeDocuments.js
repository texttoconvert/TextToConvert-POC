const util = require('util');
const Exec = util.promisify(require('child_process').exec);

module.exports = {
    MainComposer: function(inputText, inputFormat, OutputFormat) {
        return Exec('echo "' + inputText + '" | pandoc -f ' + inputFormat +
        //' -t ' + OutputFormat + 
        ' -o ./public/tempFile.' + OutputFormat);
    }
}
