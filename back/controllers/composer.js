const ServiceComposer = require('../services/composeDocuments.js');
const PUBLIC_FOLDER = '../public/';

module.exports.compose = async (req, res, next) => {
   
    ServiceComposer.MainComposer(req.body.textToConvert, req.body.inputFormat, req.body.outPutFormat).then((stdout) => {
        console.log(stdout); 
        // req.body
        var options = {
            root: __dirname + '/../public/',
            headers: {
                'x-timestamp': Date.now(),
                'x-sent': true
            }
        };
    
        res.sendFile('tempFile.' + req.body.outPutFormat, options, function (err) {
            if (err) {
                console.log(err);
                next(err);
            } else {
                console.log('Sent: tempFile.', req.body.outPutFormat);
            }
        });
    }, (error) => {
        res.sendStatus(500);
        console.error(error);
    });
};
