const express = require('express');
const router = express.Router();
const composer = require('../controllers/composer.js')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Hello World from Awesome App !');
});

router.post('/compose', composer.compose);

module.exports = router;
